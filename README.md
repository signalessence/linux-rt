# linux-rt, a working real-time kernel for working with SignalEssence XMOS based hardware.

This repo holds the config file and build products for a small kernel that works for our use.  This is a 4.18.16 kernel, with real-time patches, compiled with PREEMPT_RT enabled.  It works on our compuers and for our purposes, and that's all it's supposed to do :-)

## Instructions for using this precompiled kernel
This should do the trick:

```bash
# sudo cp -a 4.18.16-rt9se+/modules/4.18.16-rt9se+ /lib/modules
# sudo cp 4.18.16-rt9se+/config-4.18.16-rt9se+ /boot
# sudo cp 4.18.16-rt9se+/vmlinuz-4.18.16-rt9se+ /boot
# sudo update-initramfs -k 4.18.16-rt9se+ -c
# sudo update-grub
# sudo reboot
```

That should get you to this realtime kernel.  If it works/doesn't work, let me know.

## Instructions for compiling
Grab the kernel, the realtime patch, and configure the kernel:
```bash
# cd ..
# git clone git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
# cd linux-stable.git
# wget https://mirrors.edge.kernel.org/pub/linux/kernel/projects/rt/4.18/patch-4.18.16-rt9.patch.gz
# gunzip -c patch-4.18.16-rt9.patch.gz | patch -p1
# cp ../linux-rt/4.18.16-rt9se+/config-4.18.16-rt9se+ .config
```

And finally, make it:
```bash
# make -j8 bzImage modules
# make -j8 modules_install INSTALL_MOD_PATH=/home/xxx/modules
```
